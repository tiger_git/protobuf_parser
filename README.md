# protobuf_parser

#### 项目介绍
使用protobuf自带的反射功能，解析*.proto文件。实现了在不编译*.proto文件的情况下，对message进行取值，赋值，序列化，反序列化等操作。支持为win32和linux双平台。

#### 软件架构
本项目主要是对libprotobuf中原有的功能做了二次封装，提供能更加简单的接口。
#### 安装教程
主要代码都放在 protobuf_parser.h 和 protobuf_parser.cpp文件中，包含protobuf的头文件，链接libprotobuf库即可使用。所有代码都在VS2017和GCC8.1下编译通过。
在编译之前请先到 thirdparty/protobuf 目录下，运行build.bat或者build.sh,编译出本地的protobuf库。
#### 使用说明
    using namespace google::protobuf;
    using namespace google::protobuf::compiler;
    protobuf_parser pbpsr;
    // 添加proto文件所在的路径，相当于调用protoc时指定--proto_path，可以添加多个路径
    pbpsr.add_path("../proto_files");
    {
        // proto文件相对路径，相对于上面添加的proto路径
        const std::string proto_file = "proto1/msg_1.proto";
        if (false == pbpsr.parse_proto(proto_file))
        {
            std::cerr << "prase " << proto_file << " failed" << std::endl;
            return -1;
        }
    }
    {
        // proto文件相对路径，相对于上面添加的proto路径
        const std::string proto_file = "proto2/msg_2.proto";
        if (false == pbpsr.parse_proto(proto_file))
        {
            std::cerr << "prase " << proto_file << " failed" << std::endl;
            return -1;
        }
    }
    // 创建一条消息
    const std::string msg_name = "SubMsgExample";
    Message * msg = pbpsr.create_msg(msg_name);
    // 按照字段名，添加一个字段
    pbpsr.add_int32(msg, "int32_val_list", 1);
    // 按照字段id，添加一个字段
    pbpsr.add_int32(msg, 502, 2);
    dump_msg(msg); // 输出 : SubMsgExample{ int32_val_list: 1 int32_val_list: 2 }

