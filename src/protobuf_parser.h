#ifndef PROTO_PARSER_H_
#define PROTO_PARSER_H_

#include <cstdint>
#include "google/protobuf/message.h"
#include "google/protobuf/descriptor.h"
#include "google/protobuf/compiler/importer.h"
#include "google/protobuf/dynamic_message.h"

#define CHECKPTR_NULL_RETURN(ptr, ret) do{ if(ptr == nullptr){ std::cerr << #ptr" == nullptr, line=" << __LINE__ <<", func=" << __FUNCTION__ << std::endl; return ret; } } while (false)

class error_printer;

// proto文件解析器，一次只能解析一个文件，自动处理包含(import)关系
class protobuf_parser
{
public:
    protobuf_parser();
    /**
    * @brief 添加proto文件所在的目录
    * @param proto_path proto文件所在的目录
    * @return
    */
    void add_path(const std::string & proto_path);

    /**
    * @brief 解析一个文件
    * @param proto_file_name proto文件所的名字，不带目录
    * @return
    */
    bool parse_proto(const std::string & proto_file_name);

    /**
    * @brief 根据字段名判断消息对象中是否设置了某个字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 是否设置了该字段
    */
    bool has_field(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段id判断消息对象中是否设置了某个字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 是否设置了该字段
    */
    bool has_field(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段名获取某个(repeated)字段的元素个数
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 是否设置了该字段
    */
    int32_t field_size(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段id获取某个(repeated)字段的元素个数
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 是否设置了该字段
    */
    int32_t field_size(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据指定的字段名清除字段
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 是否成功
    */
    bool clear_field(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据指定的字段id清除字段
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 是否成功
    */
    bool clear_field(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据消息名创建一个消息，如果有包名，需要包含包名，例如Client.player
    * @param msg_name 消息名称
    * @return 创建的消息
    */
    google::protobuf::Message * create_msg(const std::string & msg_name) const;

    /**
    * @brief 根据字段名获取一个message类型字段的指针(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段指针
    */
    google::protobuf::Message * mutable_msg(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段id获取一个message类型字段的指针(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 字段指针
    */
    google::protobuf::Message * mutable_msg(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段名获取一个message类型字段的指针(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段指针
    */
    google::protobuf::Message * mutable_repeated_msg(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段id获取一个message类型字段的指针(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 字段指针
    */
    google::protobuf::Message * mutable_repeated_msg(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段名获取一个message类型字段的指针(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段常量引用
    */
    const google::protobuf::Message * get_repeated_msg(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段id获取一个message类型字段的指针(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 字段常量引用
    */
    const google::protobuf::Message * get_repeated_msg(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据指定的字段名设置message字段(optional/required)
    * @param msg 消息对象
    * @param sub_msg 子消息对象
    * @param field_name 字段名称
    * @return 是否成功
    */
    bool set_msg(google::protobuf::Message * msg, google::protobuf::Message * sub_msg, const std::string & field_name) const;

    /**
    * @brief 根据指定的字段id设置message字段(optional/required)
    * @param msg 消息对象
    * @param sub_msg 子消息对象
    * @param field_id 字段id
    * @return 是否成功
    */
    bool set_msg(google::protobuf::Message * msg, google::protobuf::Message * sub_msg, const int32_t field_id) const;

    /**
    * @brief 根据字段名添加一个message类型字段的指针(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段指针
    */
    google::protobuf::Message * add_msg(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段id添加一个message类型字段的指针(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 字段指针
    */
    google::protobuf::Message * add_msg(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段名添加一个message类型字段的指针(repeated)，注意，这里仅仅是一个【浅拷贝】操作
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param sub_msg 被添加的子消息
    * @return 是否成功
    */
    bool add_msg(google::protobuf::Message * msg, const std::string & field_name, google::protobuf::Message * sub_msg) const;

    /**
    * @brief 根据字段id添加一个message类型字段的指针(repeated)，注意，这里仅仅是一个【浅拷贝】操作
    * @param msg 消息对象
    * @param field_id 字段id
    * @param sub_msg 被添加的子消息
    * @return 是否成功
    */
    bool add_msg(google::protobuf::Message * msg, const int32_t field_id, google::protobuf::Message * sub_msg) const;

    /**
    * @brief 根据字段名称 和 枚举名称 设置枚举字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param enum_name 枚举名称
    * @param val_name 枚举值名字
    * @return 是否成功
    */
    bool set_enum(google::protobuf::Message * msg, const std::string & field_name, const std::string & enum_name, const std::string & val_name) const;

    /**
    * @brief 根据字段名称 和 枚举名称 设置枚举字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param enum_name 枚举名称
    * @param index 下标
    * @param val_name 枚举值名字
    * @return 是否成功
    */
    bool set_repeated_enum(google::protobuf::Message * msg, const std::string & field_name, const std::string & enum_name, const int32_t index, const std::string & val_name) const;

    /**
    * @brief 根据字段名称 和 枚举名称 添加枚举字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param enum_name 枚举名称
    * @param val_name 枚举值名字
    * @return 是否成功
    */
    bool add_enum(google::protobuf::Message * msg, const std::string & field_name, const std::string & enum_name, const std::string & val_name) const;

    /**
    * @brief 根据字段ID 和 枚举名称 设置枚举字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param enum_name 枚举名称
    * @param val_name 枚举值名字
    * @return 是否成功
    */
    bool set_enum(google::protobuf::Message * msg, const int32_t field_id, const std::string & enum_name, const std::string & val_name) const;

    /**
    * @brief 根据字段ID 和 枚举名称 设置枚举字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param enum_name 枚举名称
    * @param index 下标
    * @param val_name 枚举值名字
    * @return 是否成功
    */
    bool set_repeated_enum(google::protobuf::Message * msg, const int32_t field_id, const std::string & enum_name, const int32_t index, const std::string & val_name) const;

    /**
    * @brief 根据字段ID 和 枚举名称 添加枚举字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param enum_name 枚举名称
    * @param val_name 枚举值名字
    * @return 是否成功
    */
    bool add_enum(google::protobuf::Message * msg, const int32_t field_id, const std::string & enum_name, const std::string & val_name) const;

    /**
    * @brief 根据字段名称 和 枚举ID 设置枚举字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param enum_name 枚举名称
    * @param val_id 枚举值id
    * @return 是否成功
    */
    bool set_enum(google::protobuf::Message * msg, const std::string & field_name, const std::string & enum_name, const int32_t val_id) const;

    /**
    * @brief 根据字段名称 和 枚举ID 设置枚举字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param enum_name 枚举名称
    * @param index 下标
    * @param val_id 枚举值id
    * @return 是否成功
    */
    bool set_repeated_enum(google::protobuf::Message * msg, const std::string & field_name, const std::string & enum_name, const int32_t index, const int32_t val_id) const;

    /**
    * @brief 根据字段名称 和 枚举ID 添加枚举字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param enum_name 枚举名称
    * @param val_id 枚举值id
    * @return 是否成功
    */
    bool add_enum(google::protobuf::Message * msg, const std::string & field_name, const std::string & enum_name, const int32_t val_id) const;

    /**
    * @brief 根据字段ID 和 枚举ID 设置枚举字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param enum_name 枚举名称
    * @param val_id 枚举值id
    * @return 是否成功
    */
    bool set_enum(google::protobuf::Message * msg, const int32_t field_id, const std::string & enum_name, const int32_t val_id) const;

    /**
    * @brief 根据字段ID 和 枚举ID 设置枚举字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param enum_name 枚举名称
    * @param index 下标
    * @param val_id 枚举值id
    * @return 是否成功
    */
    bool set_repeated_enum(google::protobuf::Message * msg, const int32_t field_id, const std::string & enum_name, const int32_t index, const int32_t val_id) const;

    /**
    * @brief 根据字段ID 和 枚举ID 添加枚举字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param enum_name 枚举名称
    * @param val_id 枚举值id
    * @return 是否成功
    */
    bool add_enum(google::protobuf::Message * msg, const int32_t field_id, const std::string & enum_name, const int32_t val_id) const;

    /**
    * @brief 根据字段名 获取(optional/required)枚举字段的 枚举值
    * @param msg 消息对象
    * @param field_name 字段名
    * @return 枚举值
    */
    int32_t get_enum_number(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名 获取(repeated)枚举字段的 枚举值
    * @param msg 消息对象
    * @param field_name 字段名
    * @param index 下标
    * @return 枚举值
    */
    int32_t get_repeated_enum_number(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段id 获取(optional/required)枚举字段的 枚举值
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 枚举值
    */
    int32_t get_enum_number(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段id 获取(repeated)枚举字段的 枚举值
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @return 枚举值
    */
    int32_t get_repeated_enum_number(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段名 获取(optional/required)枚举字段的 枚举名
    * @param msg 消息对象
    * @param field_name 字段名
    * @return 枚举名
    */
    std::string get_enum_name(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名 获取(repeated)枚举字段的 枚举名
    * @param msg 消息对象
    * @param field_name 字段名
    * @param index 下标
    * @return 枚举名
    */
    std::string get_repeated_enum_name(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段id 获取枚举字段的 枚举名
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 枚举名
    */
    std::string get_enum_name(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段id 获取(repeated)枚举字段的 枚举名
    * @param msg 消息对象
    * @param field_name 字段名
    * @param index 下标
    * @return 枚举名
    */
    std::string get_repeated_enum_name(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段名称 设置string/byte字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param str_val string值
    * @return 是否成功
    */
    bool set_string(google::protobuf::Message * msg, const std::string & field_name, const std::string & str_val) const;

    /**
    * @brief 根据字段名称 获取string/byte字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段值
    */
    std::string get_string(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名称 设置string/byte字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param str_val string值
    * @return 是否成功
    */
    bool set_repeated_string(google::protobuf::Message * msg, const std::string & field_name, const int32_t index, const std::string & str_val) const;

    /**
    * @brief 根据字段名称 获取string/byte字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @return 字段值
    */
    std::string get_repeated_string(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加string/byte字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param str_val string值
    * @return 是否成功
    */
    bool add_string(google::protobuf::Message * msg, const std::string & field_name, const std::string & str_val) const;

    /**
    * @brief 根据字段ID 设置string/byte字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param str_val string值
    * @return 是否成功
    */
    bool set_string(google::protobuf::Message * msg, const int32_t field_id, const std::string & str_val) const;

    /**
    * @brief 根据字段ID 获取string/byte字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 字段值
    */
    std::string get_string(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段ID 设置string/byte字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param str_val string值
    * @return 是否成功
    */
    bool set_repeated_string(google::protobuf::Message * msg, const int32_t field_id, const int32_t index, const std::string & str_val) const;

    /**
    * @brief 根据字段ID 获取string/byte字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @return 字段值
    */
    std::string get_repeated_string(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段ID 添加string/byte字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param str_val string值
    * @return 是否成功
    */
    bool add_string(google::protobuf::Message * msg, const int32_t field_id, const std::string & str_val) const;

    /**
    * @brief 根据字段名称 设置float字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param float_val float值
    * @return 是否成功
    */
    bool set_float(google::protobuf::Message * msg, const std::string & field_name, const float float_val) const;

    /**
    * @brief 根据字段名称 获取float字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段值
    */
    float get_float(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名称 设置float字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param float_val float值
    * @return 是否成功
    */
    bool set_repeated_float(google::protobuf::Message * msg, const std::string & field_name, const int32_t index, const float float_val) const;

    /**
    * @brief 根据字段名称 获取float字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @return 字段值
    */
    float get_repeated_float(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加float字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param float_val float值
    * @return 是否成功
    */
    bool add_float(google::protobuf::Message * msg, const std::string & field_name, const float float_val) const;

    /**
    * @brief 根据字段ID 设置float字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param float_val float值
    * @return 是否成功
    */
    bool set_float(google::protobuf::Message * msg, const int32_t field_id, const float float_val) const;

    /**
    * @brief 根据字段ID 获取float字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段值
    */
    float get_float(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段ID 设置float字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param float_val float值
    * @return 是否成功
    */
    bool set_repeated_float(google::protobuf::Message * msg, const int32_t field_id, const int32_t index, const float float_val) const;

    /**
    * @brief 根据字段ID 获取float字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @return 字段值
    */
    float get_repeated_float(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段ID 添加float字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param float_val float值
    * @return 是否成功
    */
    bool add_float(google::protobuf::Message * msg, const int32_t field_id, const float float_val) const;

    /**
    * @brief 根据字段名称 设置double字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param double_val double值
    * @return 是否成功
    */
    bool set_double(google::protobuf::Message * msg, const std::string & field_name, const double double_val) const;

    /**
    * @brief 根据字段名称 获取double字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段值
    */
    double get_double(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名称 设置double字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param double_val double值
    * @return 是否成功
    */
    bool set_repeated_double(google::protobuf::Message * msg, const std::string & field_name, const int32_t index, const double double_val) const;

    /**
    * @brief 根据字段名称 获取double字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @return 是否成功
    */
    double get_repeated_double(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加double字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param double_val double值
    * @return 是否成功
    */
    bool add_double(google::protobuf::Message * msg, const std::string & field_name, const double double_val) const;

    /**
    * @brief 根据字段ID 设置double字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param double_val double值
    * @return 是否成功
    */
    bool set_double(google::protobuf::Message * msg, const int32_t field_id, const double double_val) const;

    /**
    * @brief 根据字段ID 获取double字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 字段值
    */
    double get_double(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段ID 设置double字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param double_val double值
    * @return 是否成功
    */
    bool set_repeated_double(google::protobuf::Message * msg, const int32_t field_id, const int32_t index, const double double_val) const;

    /**
    * @brief 根据字段ID 获取double字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @return 字段值
    */
    double get_repeated_double(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段ID 添加double字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param double_val double值
    * @return 是否成功
    */
    bool add_double(google::protobuf::Message * msg, const int32_t field_id, const double double_val) const;

    /**
    * @brief 根据字段名称 设置int32/sint32/sfixed32字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param int32_val int32值
    * @return 是否成功
    */
    bool set_int32(google::protobuf::Message * msg, const std::string & field_name, const int32_t int32_val) const;

    /**
    * @brief 根据字段名称 获取int32/sint32/sfixed32字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段值
    */
    int32_t get_int32(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名称 设置int32/sint32/sfixed32字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param int32_val int32值
    * @return 是否成功
    */
    bool set_repeated_int32(google::protobuf::Message * msg, const std::string & field_name, const int32_t index, const int32_t int32_val) const;

    /**
    * @brief 根据字段名称 获取int32/sint32/sfixed32字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @return 字段值
    */
    int32_t get_repeated_int32(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加int32/sint32/sfixed32字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param int32_val int32值
    * @return 是否成功
    */
    bool add_int32(google::protobuf::Message * msg, const std::string & field_name, const int32_t int32_val) const;

    /**
    * @brief 根据字段ID 设置int32/sint32/sfixed32字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param int32_val int32值
    * @return 是否成功
    */
    bool set_int32(google::protobuf::Message * msg, const int32_t field_id, const int32_t int32_val) const;

    /**
    * @brief 根据字段名称 获取int32/sint32/sfixed32字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 字段值
    */
    int32_t get_int32(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段名称 设置int32/sint32/sfixed32字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param int32_val int32值
    * @return 是否成功
    */
    bool set_repeated_int32(google::protobuf::Message * msg, const int32_t field_id, const int32_t index, const int32_t int32_val) const;

    /**
    * @brief 根据字段名称 获取int32/sint32/sfixed32字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @return 字段值
    */
    int32_t get_repeated_int32(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加int32/sint32/sfixed32字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param int32_val int32值
    * @return 是否成功
    */
    bool add_int32(google::protobuf::Message * msg, const int32_t field_id, const int32_t int32_val) const;

    /**
    * @brief 根据字段名称 设置uint32/fixed32字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param uint32_val uint32值
    * @return 是否成功
    */
    bool set_uint32(google::protobuf::Message * msg, const std::string & field_name, const uint32_t uint32_val) const;

    /**
    * @brief 根据字段名称 获取uint32/fixed32字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段值
    */
    uint32_t get_uint32(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名称 设置uint32/fixed32字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param uint32_val uint32值
    * @return 是否成功
    */
    bool set_repeated_uint32(google::protobuf::Message * msg, const std::string & field_name, const int32_t index, const uint32_t uint32_val) const;

    /**
    * @brief 根据字段名称 获取uint32/fixed32字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param uint32_val uint32值
    * @return 字段值
    */
    uint32_t get_repeated_uint32(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加uint32/fixed32字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param uint32_val uint32值
    * @return 是否成功
    */
    bool add_uint32(google::protobuf::Message * msg, const std::string & field_name, const uint32_t uint32_val) const;

    /**
    * @brief 根据字段ID 设置uint32/fixed32字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param uint32_val uint32值
    * @return 是否成功
    */
    bool set_uint32(google::protobuf::Message * msg, const int32_t field_id, const uint32_t uint32_val) const;

    /**
    * @brief 根据字段名称 获取uint32/fixed32字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 字段值
    */
    uint32_t get_uint32(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段名称 设置uint32/fixed32字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param uint32_val uint32值
    * @return 是否成功
    */
    bool set_repeated_uint32(google::protobuf::Message * msg, const int32_t field_id, const int32_t index, const uint32_t uint32_val) const;

    /**
    * @brief 根据字段名称 获取uint32/fixed32字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param uint32_val uint32值
    * @return 字段值
    */
    uint32_t get_repeated_uint32(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加uint32/fixed32字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param uint32_val uint32值
    * @return 是否成功
    */
    bool add_uint32(google::protobuf::Message * msg, const int32_t field_id, const uint32_t uint32_val) const;

    /**
    * @brief 根据字段名称 设置int64/sint64/sfixed64字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param int64_val int64值
    * @return 是否成功
    */
    bool set_int64(google::protobuf::Message * msg, const std::string & field_name, const int64_t int64_val) const;

    /**
    * @brief 根据字段名称 获取int64/sint64/sfixed64字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 字段值
    */
    int64_t get_int64(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名称 设置int64/sint64/sfixed64字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param int64_val int64值
    * @return 是否成功
    */
    bool set_repeated_int64(google::protobuf::Message * msg, const std::string & field_name, const int32_t index, const int64_t int64_val) const;

    /**
    * @brief 根据字段名称 获取int64/sint64/sfixed64字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @return 字段值
    */
    int64_t get_repeated_int64(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加int64/sint64/sfixed64字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param int64_val int64值
    * @return 是否成功
    */
    bool add_int64(google::protobuf::Message * msg, const std::string & field_name, const int64_t int64_val) const;

    /**
    * @brief 根据字段ID 设置int64/sint64/sfixed64字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param int64_val int64值
    * @return 是否成功
    */
    bool set_int64(google::protobuf::Message * msg, const int32_t field_id, const int64_t int64_val) const;

    /**
    * @brief 根据字段名称 获取int64/sint64/sfixed64字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 字段值
    */
    int64_t get_int64(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段名称 设置int64/sint64/sfixed64字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param int64_val int64值
    * @return 是否成功
    */
    bool set_repeated_int64(google::protobuf::Message * msg, const int32_t field_id, const int32_t index, const int64_t int64_val) const;

    /**
    * @brief 根据字段名称 获取int64/sint64/sfixed64字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param int64_val int64值
    * @return 字段值
    */
    int64_t get_repeated_int64(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加int64/sint64/sfixed64字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param int64_val int64值
    * @return 是否成功
    */
    bool add_int64(google::protobuf::Message * msg, const int32_t field_id, const int64_t int64_val) const;

    /**
    * @brief 根据字段名称 设置uint64/fixed64字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param uint64_val uint64值
    * @return 是否成功
    */
    bool set_uint64(google::protobuf::Message * msg, const std::string & field_name, const uint64_t uint64_val) const;

    /**
    * @brief 根据字段名称 获取uint64/fixed64字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @return 是否成功
    */
    uint64_t get_uint64(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名称 设置uint64/fixed64字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param uint64_val uint64值
    * @return 是否成功
    */
    bool set_repeated_uint64(google::protobuf::Message * msg, const std::string & field_name, const int32_t index, const uint64_t uint64_val) const;

    /**
    * @brief 根据字段名称 获取uint64/fixed64字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @return 是否成功
    */
    uint64_t get_repeated_uint64(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段名称 设置uint64/fixed64字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param uint64_val uint64值
    * @return 是否成功
    */
    bool add_uint64(google::protobuf::Message * msg, const std::string & field_name, const uint64_t uint64_val) const;

    /**
    * @brief 根据字段ID 设置uint64/fixed64字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param uint64_val uint64值
    * @return 是否成功
    */
    bool set_uint64(google::protobuf::Message * msg, const int32_t field_id, const uint64_t uint64_val) const;

    /**
    * @brief 根据字段名称 获取uint64/fixed64字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @return 是否成功
    */
    uint64_t get_uint64(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段名称 设置uint64/fixed64字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param uint64_val uint64值
    * @return 是否成功
    */
    bool set_repeated_uint64(google::protobuf::Message * msg, const int32_t field_id, const int32_t index, const uint64_t uint64_val) const;

    /**
    * @brief 根据字段名称 获取uint64/fixed64字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @return 是否成功
    */
    uint64_t get_repeated_uint64(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段名称 设置uint64/fixed64字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param uint64_val uint64值
    * @return 是否成功
    */
    bool add_uint64(google::protobuf::Message * msg, const int32_t field_id, const uint64_t uint64_val) const;

    /**
    * @brief 根据字段名称 设置bool字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param bool_val bool值
    * @return 是否成功
    */
    bool set_bool(google::protobuf::Message * msg, const std::string & field_name, const bool bool_val) const;

    /**
    * @brief 根据字段名称 设置bool字段(optional/required)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param bool_val bool值
    * @return 字段值
    */
    bool get_bool(google::protobuf::Message * msg, const std::string & field_name) const;

    /**
    * @brief 根据字段名称 设置bool字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param bool_val bool值
    * @return 是否成功
    */
    bool set_repeated_bool(google::protobuf::Message * msg, const std::string & field_name, const int32_t index, const bool bool_val) const;

    /**
    * @brief 根据字段名称 获取bool字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @param bool_val bool值
    * @return 字段值
    */
    bool get_repeated_bool(google::protobuf::Message * msg, const std::string & field_name, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加bool字段(repeated)
    * @param msg 消息对象
    * @param field_name 字段名称
    * @param index 下标
    * @return 是否成功
    */
    bool add_bool(google::protobuf::Message * msg, const std::string & field_name, const bool bool_val) const;

    /**
    * @brief 根据字段ID 设置bool字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param bool bool值
    * @return 是否成功
    */
    bool set_bool(google::protobuf::Message * msg, const int32_t field_id, const bool bool_val) const;

    /**
    * @brief 根据字段名称 设置bool字段(optional/required)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param bool_val bool值
    * @return 字段值
    */
    bool get_bool(google::protobuf::Message * msg, const int32_t field_id) const;

    /**
    * @brief 根据字段名称 设置bool字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param bool_val bool值
    * @return 是否成功
    */
    bool set_repeated_bool(google::protobuf::Message * msg, const int32_t field_id, const int32_t index, const bool bool_val) const;

    /**
    * @brief 根据字段名称 获取bool字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @param bool_val bool值
    * @return 字段值
    */
    bool get_repeated_bool(google::protobuf::Message * msg, const int32_t field_id, const int32_t index) const;

    /**
    * @brief 根据字段名称 添加bool字段(repeated)
    * @param msg 消息对象
    * @param field_id 字段id
    * @param index 下标
    * @return 是否成功
    */
    bool add_bool(google::protobuf::Message * msg, const int32_t field_id, const bool bool_val) const;
private:
    std::shared_ptr<google::protobuf::compiler::DiskSourceTree> source_tree_;
    std::shared_ptr<error_printer> error_printer_;
    std::shared_ptr<google::protobuf::compiler::Importer> importer_;
    std::shared_ptr<google::protobuf::DynamicMessageFactory> dynamic_message_factory_;
};

// 错误打印器，在解析proto的过程中，如果proto内容不正确，将会打印出错误信息
class error_printer : public google::protobuf::compiler::MultiFileErrorCollector
{
public:
    error_printer(google::protobuf::compiler::DiskSourceTree *tree = nullptr) : tree_(tree), found_errors_(false) {}
    ~error_printer() {}
    void AddError(const std::string & filename, int line, int column, const std::string & message)
    {
        found_errors_ = true;
        AddErrorOrWarning(filename, line, column, message, "error", std::cerr);
    }

    void AddWarning(const std::string& filename, int line, int column, const std::string& message)
    {
        AddErrorOrWarning(filename, line, column, message, "warning", std::clog);
    }

    bool FoundErrors() const
    {
        return found_errors_;
    }

private:
    void AddErrorOrWarning(const std::string & filename, int line, int column, const std::string & message, const std::string & type, std::ostream& out)
    {
        out << "[" << type << "] ";
        std::string disk_file;
        if(tree_ && tree_->VirtualFileToDiskFile(filename, &disk_file))
        {
            out << disk_file << ":";
        }
        else
        {
            out << filename << ":";
        }
        if(line != -1)
        {
            out << line << ":" << column << ":";
        }
        out << " " << message << std::endl;
    }
private:
    google::protobuf::compiler::DiskSourceTree *tree_;
    bool found_errors_;
};

#endif
