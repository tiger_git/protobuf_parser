#ifndef TEST_H_
#define TEST_H_
#include "protobuf_parser.h"

std::string random_msg_name();

void dump_msg(const google::protobuf::Message * msg);
void dump_msg(const std::shared_ptr<google::protobuf::Message> msg);

int32_t test_msg_func(const protobuf_parser & pbpsr);
int32_t test_enum_func(const protobuf_parser & pbpsr);
int32_t test_string_func(const protobuf_parser & pbpsr);
int32_t test_float_func(const protobuf_parser & pbpsr);
int32_t test_double_func(const protobuf_parser & pbpsr);
int32_t test_int32_func(const protobuf_parser & pbpsr);
int32_t test_uint32_func(const protobuf_parser & pbpsr);
int32_t test_int64_func(const protobuf_parser & pbpsr);
int32_t test_uint64_func(const protobuf_parser & pbpsr);
int32_t test_bool_func(const protobuf_parser & pbpsr);

int32_t test_serialize_and_parse(const protobuf_parser & pbpsr);

#endif