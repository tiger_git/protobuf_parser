#include <iostream>
#include <cstdint>
#include <string>
#include <sstream>
#include "google/protobuf/descriptor.h"
#include "google/protobuf/compiler/importer.h"
#include "google/protobuf/dynamic_message.h"
#include "protobuf_parser.h"
#include "test.h"

#define CHECK_NULL_RETURN(ptr, ret) if(ptr == nullptr){std::cerr << #ptr" == nullptr" << std::endl;return ret;}


int32_t main(int32_t argc, char * argv[])
{
    using namespace google::protobuf;
    using namespace google::protobuf::compiler;

    protobuf_parser pbpsr;
    // 添加proto文件所在的路径，相当于调用protoc时指定--proto_path，可以添加多个路径
    pbpsr.add_path("../proto_files");
    {
        // proto文件相对路径，相对于上面添加的proto路径
        const std::string proto_file = "proto1/msg_1.proto";
        if (false == pbpsr.parse_proto(proto_file))
        {
            std::cerr << "prase " << proto_file << " failed" << std::endl;
            return -1;
        }
    }
    {
        // proto文件相对路径，相对于上面添加的proto路径
        const std::string proto_file = "proto2/msg_2.proto";
        if (false == pbpsr.parse_proto(proto_file))
        {
            std::cerr << "prase " << proto_file << " failed" << std::endl;
            return -1;
        }
    }
    // 创建一条消息
    const std::string msg_name = "SubMsgExample";
    Message * msg = pbpsr.create_msg(msg_name);
    // 按照字段名，添加一个字段
    pbpsr.add_int32(msg, "int32_val_list", 1);
    // 按照字段id，添加一个字段
    pbpsr.add_int32(msg, 502, 2);
    dump_msg(msg); // 输出 : SubMsgExample{ int32_val_list: 1 int32_val_list: 2 }


    test_msg_func(pbpsr);
    test_enum_func(pbpsr);
    test_string_func(pbpsr);
    test_float_func(pbpsr);
    test_double_func(pbpsr);
    test_int32_func(pbpsr);
    test_uint32_func(pbpsr);
    test_int64_func(pbpsr);
    test_uint64_func(pbpsr);
    test_bool_func(pbpsr);
    test_serialize_and_parse(pbpsr);
    return 0;
}