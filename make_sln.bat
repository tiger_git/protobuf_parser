@echo off
set "WORKING_DIR=%~dp0"

set "PROTOC=%WORKING_DIR%tools\protoc.exe"
set "PROTO_PATH=%WORKING_DIR%proto_files"
set "CPP_OUT_PATH=%WORKING_DIR%src\pb"

if not exist %CPP_OUT_PATH% md %CPP_OUT_PATH%

for /R %PROTO_PATH%\comm %%f in (*.proto) do (
	echo %%f
	%PROTOC% --proto_path=%PROTO_PATH% --cpp_out=%CPP_OUT_PATH% %%f
)
for /R %PROTO_PATH%\proto1 %%f in (*.proto) do (
	echo %%f
	%PROTOC% --proto_path=%PROTO_PATH% --cpp_out=%CPP_OUT_PATH% %%f
)
for /R %PROTO_PATH%\proto2 %%f in (*.proto) do (
	echo %%f
	%PROTOC% --proto_path=%PROTO_PATH% --cpp_out=%CPP_OUT_PATH% %%f
)
set "CMAKE=cmake"
set "SRC_DIR=%WORKING_DIR%\src"
set "SLN_DIR=%WORKING_DIR%\sln"
if not exist %SLN_DIR% md %SLN_DIR%
cd %SLN_DIR%
%CMAKE% %SRC_DIR%
pause