#!/bin/bash
WORKING_DIR=$(cd `dirname $0`; pwd)
PROTOC=$WORKING_DIR/tools/protoc
PROTO_PATH=$WORKING_DIR/proto_files
CPP_OUT_PATH=$WORKING_DIR/src/pb
echo $PROTOC
echo $PROTO_PATH

if [ ! -d $CPP_OUT_PATH ]; then
	mkdir -p $CPP_OUT_PATH
fi
chmod +x $PROTOC

for file in `ls $PROTO_PATH/comm`
do
	echo $file
	$PROTOC --proto_path=$PROTO_PATH --cpp_out=$CPP_OUT_PATH comm/$file
done

for file in `ls $PROTO_PATH/proto1`
do
	echo $file
	$PROTOC --proto_path=$PROTO_PATH --cpp_out=$CPP_OUT_PATH proto1/$file
done

for file in `ls $PROTO_PATH/proto2`
do
	echo $file
	$PROTOC --proto_path=$PROTO_PATH --cpp_out=$CPP_OUT_PATH proto2/$file
done

SRC_DIR=$WORKING_DIR/src
BUILD_RELEASE_DIR=$WORKING_DIR/build_release
if [ ! -d $BUILD_RELEASE_DIR ]; then
	mkdir -p $BUILD_RELEASE_DIR
fi
cd $BUILD_RELEASE_DIR
cmake $SRC_DIR
make
