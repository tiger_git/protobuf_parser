echo off
set "WORKING_DIR=%~dp0"
echo %WORKING_DIR%
set "ZIP=%WORKING_DIR%..\..\tools\7z.exe"
echo %ZIP%
set "PROTOBUF_ZIP_FILE=%WORKING_DIR%protobuf-all-3.5.0.zip"
set "PROTOBUF_SRC_FILE=%WORKING_DIR%protobuf-3.5.0"
echo %PROTOBUF_ZIP_FILE%

if not exist %PROTOBUF_SRC_FILE% (
	%ZIP% x %PROTOBUF_ZIP_FILE% -y -o%WORKING_DIR%
)

cd %WORKING_DIR%\protobuf*
set "PROTOBUF_SRC_DIR=%cd%"
set "SLN_DIR=%PROTOBUF_SRC_DIR%\sln"
set "CMAKELISTS_DIR=%PROTOBUF_SRC_DIR%\cmake"

if not exist %SLN_DIR% md %SLN_DIR%

cd %SLN_DIR%
cmake %CMAKELISTS_DIR%
msbuild protobuf.sln /m /p:Configuration=Release
call extract_includes.bat
echo %cd%
xcopy %PROTOBUF_SRC_FILE%\sln\Release\libprotobuf.lib %WORKING_DIR%\..\lib /y/q
xcopy %PROTOBUF_SRC_FILE%\sln\Release\protoc.exe %WORKING_DIR%\..\..\tools /y/q
pause