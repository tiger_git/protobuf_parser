#!/bin/bash
WORKING_DIR=$(cd `dirname $0`; pwd)
PROTOBUF_ZIP_FILE=$WORKING_DIR/protobuf-all-3.5.0.zip
PROTOBUF_SRC_FILE=$WORKING_DIR/protobuf-3.5.0
if [ ! -d $PROTOBUF_SRC_FILE ]; then
	unzip $PROTOBUF_ZIP_FILE
fi

CMAKELISTS_DIR=$PROTOBUF_SRC_FILE/cmake
BUILD_DIR=$PROTOBUF_SRC_FILE/build
if [ ! -d $BUILD_DIR ]; then
	mkdir -p $BUILD_DIR
fi
cd $BUILD_DIR
cmake $CMAKELISTS_DIR -DCMAKE_BUILD_TYPE=Release
make
cp -f $PROTOBUF_SRC_FILE/build/libprotobuf.a $WORKING_DIR/../lib
cp -f $PROTOBUF_SRC_FILE/build/protoc $WORKING_DIR/../../tools
